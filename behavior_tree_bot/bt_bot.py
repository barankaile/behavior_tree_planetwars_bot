#!/usr/bin/env python
#

"""
// There is already a basic strategy in place here. You can use it as a
// starting point, or you can throw it out entirely and replace it with your
// own.
"""
import logging, traceback, sys, os, inspect
logging.basicConfig(filename=__file__[:-3] +'.log', filemode='w', level=logging.DEBUG)
logging.info('Hello world 1')

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
logging.info('Hello world 2')

from behavior_tree_bot.behaviors import *
logging.info('problem1')

from behavior_tree_bot.checks import *
logging.info('problem2')

from behavior_tree_bot.bt_nodes import Selector, Sequence, Action, Check
logging.info('problem3')

from planet_wars import PlanetWars, finish_turn
logging.info('problem4')

# You have to improve this tree or create an entire new one that is capable
# of winning against all the 5 opponent bots
logging.info('problem3')

def setup_behavior_tree():
    # Top-down construction of behavior tree
    root = Selector(name='High Level Ordering of Strategies')
    
    defensive_plan = Sequence(name='Defensive Strategy')#This was implemented in an attempt to mitigate the damage done by the enemy on one of our planets.
    under_attack_check = Check(under_attack)
    defend = Action(defend_from_attack)
    defensive_plan.child_nodes = [under_attack_check, defend]

    offensive_plan = Sequence(name='Offensive Strategy')
    #largest_fleet_check = Check(have_largest_fleet)
    largest_fleet_check = Check(enemy_is_vulnerable)
    attack = Action(attack_weakest_enemy_planet)
    offensive_plan.child_nodes = [largest_fleet_check, attack]

    spread_sequence = Sequence(name='Spread Strategy')
    neutral_planet_check = Check(if_neutral_planet_available)
    spread_action = Action(spread_to_weakest_neutral_planet)
    spread_sequence.child_nodes = [neutral_planet_check, spread_action]
    
    '''steal_plan = Sequence(name='Steal Strategy')
    enemy_fleet_check = Check(enemy_sent_fleets)
    steal_action = Action(counter_steal_enemy_planet)
    steal_plan.child_nodes = [enemy_fleet_check, steal_action]'''
    
    desperado_plan = Sequence(name='Desperado Strategy')
    desperado_check = Check(under_attack)
    desperado_action = Action(desperado_attack)
    desperado_plan.child_nodes = [desperado_check, desperado_action]

    root.child_nodes = [spread_sequence, desperado_plan, defensive_plan, offensive_plan]#The ordering of these child nodes dictates what the priority is, it will try to spread to neutrals first, then defend our planets, then attack enemy

    logging.info('\n' + root.tree_to_string())
    return root

# You don't need to change this function
def do_turn(state):
    
    behavior_tree.execute(planet_wars)

if __name__ == '__main__':
    logging.basicConfig(filename=__file__[:-3] + '.log', filemode='w', level=logging.DEBUG)
    logging.error('problem5')
    
    behavior_tree = setup_behavior_tree()
    try:
        map_data = ''
        while True:
            current_line = input()
            if len(current_line) >= 2 and current_line.startswith("go"):
                planet_wars = PlanetWars(map_data)
                do_turn(planet_wars)
                finish_turn()
                map_data = ''
            else:
                map_data += current_line + '\n'

    except KeyboardInterrupt:
        print('ctrl-c, leaving ...')
    except Exception:
        traceback.print_exc(file=sys.stdout)
        logging.exception("Error in bot.")
