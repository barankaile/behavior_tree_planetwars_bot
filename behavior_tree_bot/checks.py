

def if_neutral_planet_available(state):
    if not state.my_planets():
        return False
    for planet in state.not_my_planets():
        if planet.owner == 0:
            return True
    return False
    #return any(state.neutral_planets())
def have_largest_fleet(state):
    return sum(planet.num_ships for planet in state.my_planets()) \
             + sum(fleet.num_ships for fleet in state.my_fleets()) \
           > sum(planet.num_ships for planet in state.enemy_planets()) \
             + sum(fleet.num_ships for fleet in state.enemy_fleets())
def enemy_is_vulnerable(state):
    strongest_planet = max(state.my_planets(), key=lambda t: t.num_ships, default=None)
    weakest_enemy = min(state.enemy_planets(), key=lambda t: t.num_ships, default=None)
    if not weakest_enemy:
        return True
    elif not strongest_planet:
        return False
    else:
        return strongest_planet.num_ships > weakest_enemy.num_ships
def under_attack(state):#This just checks if any of the enemy fleets are heading towards planets that we own
    attack = False
    for planet in state.my_planets():
        attack = any(fleet.destination_planet == planet.ID for fleet in state.enemy_fleets())                          
    return attack
def enemy_sent_fleets(state):
    if state.enemy_fleets():
        return True
    else:
        return False