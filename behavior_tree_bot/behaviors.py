import logging, sys
sys.path.insert(0, '../')
from planet_wars import issue_order
logging.basicConfig(filename='example.log',level=logging.DEBUG)
logging.debug('This message should go to the log file')

def attack_weakest_enemy_planet(state):
    enemy_weak_planets = sorted(state.enemy_planets(), key=lambda p: p.num_ships)
    
    easy_enemy = None
    attack_from = None
    required_ships = 9999999
    
    for planet in enemy_weak_planets:
        if any(fleet.destination_planet == planet.ID for fleet in state.my_fleets()):
            pass
        else:
            my_close_planets = sorted(state.my_planets(), key=lambda t: state.distance(planet.ID, t.ID))
            for my_planet in my_close_planets:
                required_ships = planet.num_ships + \
                (state.distance(my_planet.ID, planet.ID) * planet.growth_rate) + 1
                if required_ships < my_planet.num_ships:
                    easy_enemy = planet
                    attack_from = my_planet
                    break
            else:
                continue
            break
    if not easy_enemy or not attack_from:
        return False
    else:
        return issue_order(state, attack_from.ID, easy_enemy.ID, required_ships)

'''def counter_steal_enemy_planet(state):  # Send a fleet to take an enemy planet right as it is about to be captured
    enemy_fleets_travelling = sorted(state.enemy_fleets(), key=lambda p: p.num_ships)
    
    enemy_destination = None
    target_planet = None
    closest_planet = None
    easy_steal = None
    attack_from = None
    required_ships = 9999999
    
    for enemy_fleet in enemy_fleets_travelling:
        enemy_destination = enemy_fleet.destination_planet
        # If the fleet isn't taking a neutral planet, we don't care
        for neutral in state.neutral_planets():
            if neutral.ID == enemy_destination:
                target_planet = neutral
        if target_planet:
            my_close_planets = sorted(state.my_planets(), key=lambda t: state.distance(enemy_destination, t.ID))
            for my_planet in my_close_planets:
                # If there is a planet that can take the fleet's target right as it is captured, proceed
                if state.distance(my_planet.ID, enemy_destination) == (enemy_fleet.turns_remaining + 1):
                    # If the enemy fleet will take the planet, proceed
                    if enemy_fleet.num_ships > (target_planet.num_ships + (target_planet.growth_rate * enemy_fleet.turns_remaining)):
                        required_ships = (target_planet.num_ships + \
                        (target_planet.growth_rate * enemy_fleet.turns_remaining)) - enemy_fleet.num_ships + \
                        (target_planet.growth_rate + 1)
                        if required_ships < my_planet.num_ships:
                            easy_steal = target_planet
                            attack_from = my_planet
                            break
            else:
                continue
    if not easy_steal or not attack_from:
        return False
    else:
        return issue_order(state, attack_from.ID, easy_steal.ID, required_ships)'''
                
#def predict_enemy_attack_aftermath(state, my_planet, cost):  # Checks whether or not sending a fleet of the given cost 
        
def spread_to_weakest_neutral_planet(state):

    # (2) Find my strongest planet.
    strongest_planet = max(state.my_planets(), key=lambda p: p.num_ships, default=None)
    neutrals = sorted(state.neutral_planets(), key=lambda p: state.distance(strongest_planet.ID, p.ID))#ordering neutral planets by distance from smallest planet (ascending)
    
    min_planet = None
    for planet in neutrals:
        already_sent = False#checking if we have already sent a fleet to this planet
        for my_fleet in state.my_fleets():
            if my_fleet.destination_planet == planet.ID:
                already_sent = True
        if not already_sent:
            for fleet in state.enemy_fleets():#checking if the enemy has sent a fleet to this planet
                if fleet.destination_planet == planet: 
                    if (planet.num_ships+fleet.num_ships + 1)/strongest_planet.num_ships<= .60:#if they have, the number of ships we neede to take over the planet is neutral+enemy_fleet+1
                        min_planet = planet
                        break
            if (planet.num_ships + 1)/strongest_planet.num_ships <= .60:#otherwise if the enemy hasnt sent a fleet we send 1 more than the number of enemy ships
                min_planet = planet
                break
    if not strongest_planet or not min_planet:
        # No legal source or destination
        return False

    else:
        return issue_order(state, strongest_planet.ID, min_planet.ID, min_planet.num_ships + 1)
        

def defend_from_attack(state):
    defend_planet = None
    planet_list = state.my_planets()
    for planet in planet_list:#checking if the enemy has sent a fleet to any of our planets
        if any(fleet.destination_planet == planet.ID for fleet in state.enemy_fleets()):
            if not any(my_fleet.destination_planet == planet.ID for my_fleet in state.my_fleets()):
                for enemy in state.enemy_fleets():
                    if enemy.num_ships > (planet.num_ships/2 + (planet.growth_rate * enemy.turns_remaining)):
                        defend_planet = planet
    if defend_planet in planet_list:
        planet_list.remove(defend_planet)
    else:
        return False
    required_ships = 999999999
    close_ally = None
    while close_ally == None:
        if len(planet_list) <= 0:
            break
        close_ally = min(planet_list, key=lambda t: state.distance(defend_planet.ID, t.ID), default=None)#finding the closest planet to our planet that is under attack
        if any(fleet.destination_planet == close_ally.ID for fleet in state.enemy_fleets()):
            for fleet in state.enemy_fleets():
                if fleet.destination_planet == close_ally.ID and fleet.num_ships > (close_ally.num_ships/2 + (close_ally.growth_rate * fleet.turns_remaining)):
                    planet_list.remove(close_ally)
                else:
                    required_ships = min(fleet.num_ships,round(close_ally.num_ships/2))
                    break
    #One improvement would be to check if we have already sent a fleet to this planet, if so dont sent another one
    if not defend_planet or not close_ally:
        return False
    else:    
        return issue_order(state, close_ally.ID, defend_planet.ID, required_ships)#send half of the ships from the closest planet to the planet under attack

def desperado_attack(state):
    desperado_planet = None
    planet_list = state.my_planets()
    for planet in planet_list:  #checking if the enemy has sent a fleet to any of our planets
        if any(fleet.destination_planet == planet.ID and fleet.turns_remaining == 1 for fleet in state.enemy_fleets()):
            if any(fleet.num_ships > (planet.num_ships) for fleet in state.enemy_fleets()):
                desperado_planet = planet
    if desperado_planet in planet_list:
        planet_list.remove(desperado_planet)
    else:
        return False
    close_enemy = min(state.enemy_planets(), key=lambda t: state.distance(desperado_planet.ID, t.ID), default=None)  #finding the closest planet to our planet that is under attack
    close_neutral = min(state.neutral_planets(), key=lambda t: state.distance(desperado_planet.ID, t.ID), default=None)
    #close_ally = min(planet_list, key=lambda t: state.distance(desperado_planet.ID, t.ID), default=None)
    if close_enemy:
        if any(fleet.destination_planet == close_enemy.ID for fleet in state.my_fleets()):
            pass
        else:
            required_ships = close_enemy.num_ships + \
            (state.distance(desperado_planet.ID, close_enemy.ID) * close_enemy.growth_rate) + 1
            if required_ships < desperado_planet.num_ships:
                return issue_order(state, desperado_planet.ID, close_enemy.ID, desperado_planet.num_ships)
            else:
                pass
    if close_neutral:
        if any(fleet.destination_planet == close_neutral.ID for fleet in state.my_fleets()):
            pass
        else:
            required_ships = close_neutral.num_ships + \
            (state.distance(desperado_planet.ID, close_neutral.ID) * close_neutral.growth_rate) + 1
            if required_ships < desperado_planet.num_ships:
                return issue_order(state, desperado_planet.ID, close_neutral.ID, desperado_planet.num_ships)
            else:
                pass
    else:
        return False
    
    
    
    
    
    
    
    
    
    
    
    
    
    
